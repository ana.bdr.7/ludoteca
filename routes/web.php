<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ArtistaController;
use App\Http\Controllers\GeneroController;
use App\Http\Controllers\ObraController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ArtistaController::class,'inicio'])->name('artista.inicio');

Route::get('artistas/index',[ArtistaController::class,'index'])->name('artistas.index');

Route::get('artistas/{artista}',[ArtistaController::class,'show'])->name('artistas.show');

Route::get('generos/{genero}',[GeneroController::class,'show'])->name('generos.show');

Route::get('obras/crear',[ObraController::class,'create'])->name('obras.create');

Route::post('artistas/index',[ObraController::class,'store'])->name('obras.store');

