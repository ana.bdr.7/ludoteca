<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Database\Seeders\GeneroSeeder;
use Database\Seeders\ObraSeeder;
use Database\Seeders\ArtistaSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       
        
        DB::table('obras')->delete();
        $this->call(ObraSeeder::class); 
        DB::table('generos')->delete();
        $this->call(GeneroSeeder::class); 
        DB::table('artistas')->delete();
        $this->call(ArtistaSeeder::class); 


    }
}
