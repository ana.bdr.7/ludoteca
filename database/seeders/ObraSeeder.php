<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Obra;
use Illuminate\Support\Str;

class ObraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $obras = array(
        array(
            'nombre' => 'las meninas',
            'imagen' => 'lasmeninas.jpg'
        ),array(
            'nombre' => 'gernika',
            'imagen' => 'gernika.jpg'
        ),array(
            'nombre' => 'las tres gracias',
            'imagen' => 'lastresgracias.jpg'
        ),array(
            'nombre' => 'la gioconda',
            'imagen' => 'lagioconda.jpg'
        ),array(
            'nombre' => 'la ultima cena',
            'imagen' => 'laultimacena.jpg'
        )
        );
    public function run()
    {
        foreach($this->obras as $obra){
            $a = new Obra();
            $a->nombre = $obra['nombre'];
            $a->imagen = $obra['imagen'];
            $a->save();
        }
        $this->command->info('tabla de obras inicializada');
    }
}
