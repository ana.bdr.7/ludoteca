<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Artista;
use Illuminate\Support\Str;

class ArtistaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    private $artistas = array(
         array(
             'nombre' => 'Velazquez',
             'pais' => 'España',
             'fechaNacimiento' => '1520-05-02'
         ),array(
            'nombre' => 'Rubens',
            'pais' => 'Italia',
            'fechaNacimiento' => '1250-02-06'
        ),array(
            'nombre' => 'Picasso',
            'pais' => 'España',
            'fechaNacimiento' => '1905-06-06'
        )
    );
    public function run()
    {
        foreach($this->artistas as $artista){
            $a = new Artista();
            $a->nombre = $artista['nombre'];
            $a->pais = $artista['pais'];
            $a->fechaNacimiento = $artista['fechaNacimiento'];
            $a->slug = Str::slug($artista['nombre']);
            $a->genero1 = rand(1,5);
            $a->genero2 = rand(1,5);
            $a->genero3 = rand(1,5);
            $a->save();
        }
        $this->command->info('tabla de artistas inicializada');
    }
}
