<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Artista extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function obras(){
        return $this->hasMany(Obra::class);
    }

    public function generos(){
        return $this->belongsTo(Genero::class,'genero1')->get()
            ->merge($this->belongsTo(Genero::class,'genero2')->get())
                ->merge($this->belongsTo(Genero::class,'genero3')->get());
    }
}
