<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Obra extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function artista(){
        return $this->belongsTo(Artista::class);
    }
}
